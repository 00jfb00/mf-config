import { registerApplication, start } from "single-spa";
import Reducer from "./redux/Reducer";

function registerApplications() {
  registerApplication({
    name: "@mf/people",
    app: () => System.import("@mf/people"),
    activeWhen: "/people",
    customProps: { Reducer },
  });

  registerApplication({
    name: "@mf/planets",
    app: () => System.import("@mf/planets"),
    activeWhen: "/planets",
    customProps: { Reducer },
  });
}

registerApplication({
  name: "@mf/root",
  app: () =>
    System.import("@mf/root")
      .then((module) => module)
      .finally(() => {
        registerApplications();
      }),
  activeWhen: "/",
  customProps: { Reducer },
});

start();
